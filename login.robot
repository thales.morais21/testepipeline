*** Settings ***
Documentation     Suite de teste de logi simples no site do westwingnow, para efetuar testes dentro de uma pipeline



Library     SeleniumLibrary

Resource    common.resource
Resource    setup.resource

Test Setup        Access website
Test Teardown     Finish test  

*** Test Cases ***
Login with succes on sitte
    Input Text                          xpath=//input[@id="LoginForm_email"]                   thales.morais21@gmail.com
    Input Text                          xpath=//input[@id="LoginForm_password"]                ta211097
    Click Element                       xpath=//button[@name="login"]
    Page Should not Contain Element     xpath=//*[contains(@text, "Cadastre-se")]

   
Login with error on wrong password
    Input Text                      xpath=//input[@id="LoginForm_email"]                   thales.morais21@gmail.com
    Input Text                      xpath=//input[@id="LoginForm_password"]                12312321
    Click Element                   xpath=//button[@name="login"]
    Page Should Contain             E-mail ou senha incorretos.    


Login with error on wrong email
    Input Text                      xpath=//input[@id="LoginForm_email"]                   thales.morais2231@gmail.com
    Input Text                      xpath=//input[@id="LoginForm_password"]                ta211097
    Click Element                   xpath=//button[@name="login"]
    Page Should Contain             E-mail ou senha incorretos. 

   
